import React from 'react';
import '../styles/styles.scss';
import {HiOutlineScissors} from 'react-icons/hi';



export default class Graph extends React.Component {
    render(){
        return(
            
            <div className="firstcomp">
                
                <div className="scissor">
                <HiOutlineScissors size={25}/>
                </div>
                
                <p className="select">Selected Channel</p>


            <svg classname="graph" height="280" width="1400">
                <line x1="100" y1="10" x2="100" y2="200" stroke="grey" />
                <line x1="200" y1="10" x2="200" y2="200" stroke="grey" />
                <line x1="300" y1="10" x2="300" y2="200" stroke="grey" />
                <line x1="400" y1="10" x2="400" y2="200" stroke="grey" />
                <line x1="500" y1="10" x2="500" y2="200" stroke="grey" />
                <line x1="600" y1="10" x2="600" y2="200" stroke="grey" />
                <line x1="700" y1="10" x2="700" y2="200" stroke="grey" />
                <line x1="800" y1="10" x2="800" y2="200" stroke="grey" />
                <line x1="900" y1="10" x2="900" y2="200" stroke="grey" />
                <line x1="1000" y1="10" x2="1000" y2="200" stroke="grey" />
                <line x1="1100" y1="10" x2="1100" y2="200" stroke="grey" />
                <line x1="1200" y1="10" x2="1200" y2="200" stroke="grey" />
                <line x1="100" y1="10" x2="1200" y2="10" stroke="grey" />
                <line x1="100" y1="200" x2="1200" y2="200" stroke="grey" />
                <g classname="label"><text x="80" y="220" >10:00</text></g>
                <g classname="label"><text x="180" y="220" >10:05</text></g>
                <g classname="label"><text x="280" y="220" >10:10</text></g>
                <g classname="label"><text x="380" y="220" >10:15</text></g>
                <g classname="label"><text x="480" y="220" >10:20</text></g>
                <g classname="label"><text x="580" y="220" >10:25</text></g>
                <g classname="label"><text x="680" y="220" >10:30</text></g>
                <g classname="label"><text x="780" y="220" >10:35</text></g>
                <g classname="label"><text x="880" y="220" >10:40</text></g>
                <g classname="label"><text x="980" y="220" >10:45</text></g>
                <g classname="label"><text x="1080" y="220" >10:50</text></g>
                <g classname="label"><text x="1180" y="220" >10:55</text></g>
                <g classname="label"><text fontFamily='bold' x="680" y="270" >TIME</text></g>
                <path d="M 100 190 q 150 -300  1100 0" stroke="yellow" stroke-width="2" fill="none" />
                <path d="M 100 150 q 150 -150  1100 0" stroke="red" stroke-width="2" fill="none" />
                <path d="M 100 90 q 150 150  1100 0" stroke="purple" stroke-width="2" fill="none" />
                <path d="M 100 70 q 150 80  1100 0" stroke="blue" stroke-width="2" fill="none" />
                
                
            </svg>

            

            </div>
            
         






       

        )
    }
}