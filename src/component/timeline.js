import { useState } from "react";
import React from 'react'
import {IoCloseCircleSharp} from 'react-icons/io5';
import '../styles/styles.scss';
import {GoPlus} from 'react-icons/go';


    
function Timeline(){
    const [serviceList, setServiceList] = useState ([{BoxID: "", BoxStrt:"",BoxEnd:"",BoxMinstrt:"",Boxminend:"" },{BoxID: "", BoxStrt:"",BoxEnd:"",BoxMinstrt:"",Boxminend:"" },
    {BoxID: "", BoxStrt:"",BoxEnd:"",BoxMinstrt:"",Boxminend:"" },{BoxID: "", BoxStrt:"",BoxEnd:"",BoxMinstrt:"",Boxminend:"" },
    {BoxID: "", BoxStrt:"",BoxEnd:"",BoxMinstrt:"",Boxminend:"" },{BoxID: "", BoxStrt:"",BoxEnd:"",BoxMinstrt:"",Boxminend:"" }]);

    const handleServiceChange = (e, index) => {
        const { name, value } = e.target;
        const list = [...serviceList];
        list[index][name] = value;
        setServiceList(list);
      };

    const handleServiceRemove = (index) => {
        const list = [...serviceList];
        list.splice(index, 1);
        setServiceList(list);
      };

      const handleServiceAdd = () => {
        setServiceList([...serviceList, { BoxID: "", BoxStrt:"",BoxEnd:"",BoxMinstrt:"",Boxminend:"" }]);
      };

        return(
            
         <>
            
            <div className="timelinecomponent">
            {serviceList.map((singleService, index) => (
                      <div key={index} className="services">
                      <div className="firstbox">
                          <p className="id">ID</p>
                          <input name ="BoxID" type="text" className="inputid" 
                          value={singleService.BoxID}
                          onChange={(e) => handleServiceChange(e, index)}>
                          </input>
      
                          <p className="start">START</p>
                          <input name="BoxStrt" type="text" className="startbox"></input>
                          
      
                          <p className="end">END</p>
                          <input name="BoxEnd" type="text" className="endbox"></input>
      
                          <p className="minstart">MIN START</p>
                          <input name="BoxMinstrt" type="text" className="minstartbox"></input>
                          
      
                          <p className="minend">MIN END</p>
                          <input name="Boxminend" type="text" className="minendbox"></input>
      
                          <IoCloseCircleSharp  onClick={() => handleServiceRemove(index)} className="closetime" size={20}></IoCloseCircleSharp>
                   </div>
                    </div>

            ))}
               
            
                <button className="addbox"  onClick={handleServiceAdd} >
                    <GoPlus className="goplus" size={30}></GoPlus>
                </button> 
               
            
        </div>
        </>
          

        );

}
export default Timeline;
      