import React from 'react';
import {useState} from 'react';
import '../styles/styles.scss';
import {IoIosCloseCircleOutline} from "react-icons/io";
import {ImPlus} from "react-icons/im";

function Channel() {
    const [state, setstate] = useState(false);
    const showDropdown = ()=>{
        setstate(true);
}
    const hideDropdown=()=>{
        setstate(false);
}

        return(
            //buttons to switch between channels
            <>
            <p className="selectchannel">Selected Channels</p>
            <div className="channelborder">
                <div>
                    <button class="button"  className="first"> 
                    <IoIosCloseCircleOutline className="speedicon" size={20}></IoIosCloseCircleOutline>
                    <div className="speedtext">
                    Speed 
                    </div>
                    </button> 

                    <button  className="channelA"> 
                    <IoIosCloseCircleOutline className="resticon" size={20}></IoIosCloseCircleOutline>
                    <div className="speedtext">
                    Channel A 
                    </div>
                    </button>

                    <button  className="channelB"> 
                    <IoIosCloseCircleOutline className="resticon" size={20}></IoIosCloseCircleOutline>
                    <div className="speedtext">
                    Channel B 
                    </div>
                    </button>

                    <button  className="channelC"> 
                    <IoIosCloseCircleOutline className="resticon" size={20}></IoIosCloseCircleOutline>
                    <div className="speedtext">
                    Channel C 
                    </div>
                    </button>
                    
                    
                    <button className="pluschannel" onClick={showDropdown} onMouseLeave={hideDropdown} color="grey">
                    <ImPlus className="plusicon" size={20}></ImPlus>
                        {state ?( <ul className="dropdown-list" onClick={showDropdown}>
                            <li >Channel D</li>
                            <li>Channel E</li>
                            <li>Channel F</li>
                        </ul>):
                        null}
                        

                       </button> 
                    </div>
                    
                    
                    
                        

                    
                </div>
                
                    

            
            </>
           

        );
    }
    export default Channel;


    
    
    


